// Pin  0 -  7: PD0-7
// Pin  8 - 13: PB0-5
// Pin 14 - 19: PC0-5
// Pin 20 - 21: PE2-3
// Pin 22 - 23: PE0-1

#define DOOR_PIN 22

#define RS485_RE 19 // PC5
#define RS485_WE 2 // PD2

#define READER_RS485_RE 20 // PE2
#define READER_RS485_WE 13 // PB5
